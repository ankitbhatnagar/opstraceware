package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/ankitbhatnagar/opstraceware/pkg/tracing"
	"go.opentelemetry.io/otel"
	semconv "go.opentelemetry.io/otel/semconv/v1.10.0"
	"go.opentelemetry.io/otel/trace"
)

const (
	instrumentationName    string = "gitlab.com/gitlab-org/opstrace"
	instrumentationVersion string = "v0.0.1"
)

var (
	mode     string
	endpoint string
	urlpath  string
	tracer   = otel.GetTracerProvider().Tracer(
		instrumentationName,
		trace.WithInstrumentationVersion(instrumentationVersion),
		trace.WithSchemaURL(semconv.SchemaURL),
	)
)

func add(ctx context.Context, x, y int64) int64 {
	var span trace.Span
	_, span = tracer.Start(ctx, "Addition")
	defer span.End()

	return x + y
}

func multiply(ctx context.Context, x, y int64) int64 {
	var span trace.Span
	_, span = tracer.Start(ctx, "Multiplication")
	defer span.End()

	return x * y
}

func main() {
	flag.StringVar(&mode, "mode", "stdout", "export traces format identifier")
	flag.StringVar(&endpoint, "endpoint", "localhost", "trace endpoint")
	flag.StringVar(&urlpath, "urlpath", "/v1/traces", "trace urlpath")
	flag.Parse()

	switch mode {
	case "jaeger":
		endpoint = "http://localhost:14268/api/traces"
		urlpath = ""
	case "otel":
		endpoint = "localhost"
		urlpath = "/v1/traces"
	}

	var apiKey string
	if _, ok := os.LookupEnv("GRAFANA_API_KEY"); ok {
		apiKey = os.Getenv("GRAFANA_API_KEY")
	}
	headers := map[string]string{
		"Authorization": fmt.Sprintf("Bearer %s", apiKey),
	}

	tracerProvider, err := tracing.New(mode, endpoint, urlpath, headers)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := tracerProvider.Shutdown(context.Background()); err != nil {
			log.Printf("error shutting down tracer provider: %v", err)
		}
	}()

	ctx := context.Background()
	log.Println("answer = ", add(
		ctx, multiply(
			ctx, multiply(
				ctx, 2, 2,
			), 10), 2))
}
