package main

import (
	"bytes"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	sentry "github.com/getsentry/sentry-go"
)

const ERROR_TRACKING_API = "http://localhost:8080/errortracking/api/v1"
const PROJECT_ID int = 1
const SENTRY_SECRET string = "someSecret"

var (
	apiBaseURL   string
	projectID    int
	sentrySecret string
)

func main() {
	flag.StringVar(&apiBaseURL, "url", "https://observe.gitlab.com/errortracking/api/v1", "errortracking API base url")
	flag.IntVar(&projectID, "projectID", 37743989, "project ID")
	flag.StringVar(&sentrySecret, "secret", "", "sentry secret")
	flag.Parse()

	// not found through cli arg, check env var
	if sentrySecret == "" {
		if _, ok := os.LookupEnv("SENTRY_SECRET"); ok {
			sentrySecret = os.Getenv("SENTRY_SECRET")
		}
	}

	if sentrySecret == "" {
		log.Fatal("need sentry secret to authenticate client, didn't find any")
	}

	url := fmt.Sprintf("%s/projects/api/%d/store?sentry_key=%s",
		apiBaseURL,
		projectID,
		sentrySecret,
	)
	fmt.Printf("Requesting: %s\n", url)

	payload, err := ioutil.ReadFile("payload.json")
	if err != nil {
		log.Fatal(err)
	}

	var event sentry.Event
	if err := json.Unmarshal(payload, &event); err != nil {
		log.Fatal(err)
	}

	// generate random EventID
	b := make([]byte, 32)
	_, err = rand.Read(b)
	if err != nil {
		log.Fatal(err)
	}

	// update attributes
	event.EventID = sentry.EventID(b)
	event.Timestamp = time.Now()
	event.StartTime = time.Now()

	payload, err = json.Marshal(event)
	if err != nil {
		log.Fatal(err)
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Basic %s", base64.StdEncoding.EncodeToString([]byte(SENTRY_SECRET))))

	client := &http.Client{
		Timeout: 5 * time.Second,
	}
	fmt.Printf("sending request\n")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	fmt.Println("response status:", resp.Status)
	fmt.Println("response headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response body:", string(body))
}
