package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
)

func main() {
	var sentryDSN string
	if _, ok := os.LookupEnv("SENTRY_DSN"); ok {
		sentryDSN = os.Getenv("SENTRY_DSN")
	}
	if sentryDSN == "" {
		panic("please export your DSN as SENTRY_DSN")
	}

	err := sentry.Init(sentry.ClientOptions{
		// Either set your DSN here or set the SENTRY_DSN environment variable.
		Dsn: sentryDSN,
		// Either set environment and release here or set the SENTRY_ENVIRONMENT
		// and SENTRY_RELEASE environment variables.
		Environment: "",
		Release:     "my-project-name@1.0.0",
		// Enable printing of SDK debug messages.
		// Useful when getting started or trying to figure something out.
		Debug: true,
		// Set TracesSampleRate to 1.0 to capture 100%
		// of transactions for performance monitoring.
		// We recommend adjusting this value in production,
		TracesSampleRate: 1.0,
	})
	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}

	makeErrors := true
	if makeErrors {
		// sentry.CaptureMessage("sending a test message, this should show up in the UI")
		sentry.CaptureException(fmt.Errorf("sending a test error, this should show up in the UI"))
	}

	// Flush buffered events before the program terminates.
	// Set the timeout to the maximum duration the program can afford to wait.
	defer sentry.Flush(2 * time.Second)
}
