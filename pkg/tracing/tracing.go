package tracing

import (
	"context"
	"fmt"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	stdout "go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.10.0"
)

func Resource() *resource.Resource {
	return resource.NewWithAttributes(
		semconv.SchemaURL,
		semconv.ServiceNameKey.String("my-service-name"),
		semconv.ServiceVersionKey.String("0.0.1"),
	)
}

func New(mode, endpoint, urlpath string, headers map[string]string) (*sdktrace.TracerProvider, error) {
	var (
		destURL  string
		exporter sdktrace.SpanExporter
		err      error
	)

	switch mode {
	case "stdout":
		// output all received traces to stdout
		exporter, err = stdout.New(stdout.WithPrettyPrint())
		destURL = ""
	case "otel":
		ctx := context.Background()
		client := otlptracehttp.NewClient(
			otlptracehttp.WithEndpoint(endpoint),
			otlptracehttp.WithURLPath(urlpath),
			otlptracehttp.WithCompression(otlptracehttp.GzipCompression),
			otlptracehttp.WithHeaders(headers),
			otlptracehttp.WithInsecure(),
		)
		exporter, err = otlptrace.New(ctx, client)
		destURL = fmt.Sprintf("%s:%s", endpoint, urlpath)
	case "jaeger":
		exporter, err = jaeger.New(
			jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(endpoint)),
		)
		destURL = endpoint
	}
	if err != nil {
		return nil, err
	}

	fmt.Printf("tracing exporter configured to talk to endpoint %s in mode %s\n", destURL, mode)

	tp := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithBatcher(exporter),
		sdktrace.WithResource(Resource()),
	)

	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(
		propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}),
	)
	return tp, nil
}
